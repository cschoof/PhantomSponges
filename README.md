# Beyond PhantomSponges: Enhancing Sponge Attack on Object Detection Models
This is an implementation of Beyond PhantomSponges: Enhancing Sponge Attack on Object Detection Models by Coen Schoof, Stefanos Koffas, Mauro Conti and Stjepan Picek.

## Datasets
### Berkeley DeepDrive (BDD)
The dataset can be found [here](https://doc.bdd100k.com/download.html#k-images).

## Installation
Install the required packages in req.txt.
