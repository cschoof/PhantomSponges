print("Starting script", flush=True)
models_vers = [8] # for example: models_vers = [5] or models_vers = [3, 4, 5]
epsilon = 30 
lambda_1 = 0.60 #1
lambda_2 = 10 
seed = 42
patch_size=(640,640)
img_size=(640,640)
batch_size = 8 #8
num_workers = 4
max_labels_per_img = 65
new_bboxes_area_loss_bool = True
model_size = 'l'
dataset = 'bdd'

if dataset == 'bdd':
  IMG_DIR = '/ceph/csedu-scratch/project/cschoof/bdd_in_YOLOV5_val/images/val'
  LAB_DIR = '/ceph/csedu-scratch/project/cschoof/bdd_in_YOLOV5_val/labels/val'
elif dataset == 'mtsd':
  IMG_DIR = '/ceph/csedu-scratch/project/cschoof/MTSD/MTSD/images'
  LAB_DIR = '/ceph/csedu-scratch/project/cschoof/MTSD/MTSD/annotations'
elif dataset == 'pascal-voc':
  IMG_DIR = '/ceph/csedu-scratch/project/cschoof/PASCAL-VOC/images/val'
  LAB_DIR = '/ceph/csedu-scratch/project/cschoof/PASCAL-VOC/labels/val'
elif dataset == 'kitti':
  IMG_DIR = '/ceph/csedu-scratch/project/cschoof/KITTI/valid/images'
  LAB_DIR = '/ceph/csedu-scratch/project/cschoof/KITTI/valid/labels'
else:
  print("Please provide one of the following three datasets: 'bdd', 'mtsd', 'pascal-voc', or 'kitti'")


import torch
import os
import random
import numpy

from datasets.augmentations1 import train_transform
from datasets.split_data_set_combined import SplitDatasetCombined_BDD

def collate_fn(batch):
    return tuple(zip(*batch))

def set_random_seed(seed_value, use_cuda=True):
    numpy.random.seed(seed_value)  # cpu vars
    torch.manual_seed(seed_value)  # cpu  vars
    random.seed(seed_value)  # Python
    os.environ['PYTHONHASHSEED'] = str(seed_value)  # Python hash buildin
    if use_cuda:
        torch.cuda.manual_seed(seed_value)
        torch.cuda.manual_seed_all(seed_value)  # gpu vars
        torch.backends.cudnn.deterministic = True  # needed
        torch.backends.cudnn.benchmark = False

split_dataset = SplitDatasetCombined_BDD(
            img_dir= IMG_DIR,
            lab_dir= LAB_DIR,
            max_lab=max_labels_per_img,
            img_size=img_size,
            transform=train_transform,
            collate_fn=collate_fn)

train_loader, val_loader, test_loader = split_dataset(val_split=0.1,
                                                      shuffle_dataset=True,
                                                      random_seed=seed,
                                                      batch_size=batch_size,
                                                      ordered=False,
                                                      collate_fn=collate_fn)

import numpy
from attack.uap_phantom_sponge import UAPPhantomSponge

torch.cuda.empty_cache()

patch_name = r"yolov"
for ver in models_vers:
  patch_name += f"{ver}"
patch_name += f"_eps={epsilon}_lmbd1={lambda_1}_lmbd2={lambda_2}_newLoss={new_bboxes_area_loss_bool}_modelSize={model_size}_dataset={dataset}"

print("-------------------------------------")
print("SETUP: ")
print(f"models_vers = {models_vers}")
print(f"epsilon = {epsilon}")
print(f"lambda_1 = {lambda_1}")
print(f"lambda_2 = {lambda_2}")
print(f"new bboxes_area loss term = {new_bboxes_area_loss_bool}")
print(f"model size used (only yolov5 and up) = {model_size}")
print(f"dataset = {dataset}")
print("-------------------------------------")

uap_phantom_sponge_attack = UAPPhantomSponge(patch_folder=patch_name, 
                                            train_loader=train_loader, 
                                            val_loader=val_loader, 
                                            epsilon = epsilon, 
                                            lambda_1=lambda_1, 
                                            lambda_2=lambda_2, 
                                            patch_size=patch_size, 
                                            models_vers=models_vers,
                                            new_bboxes_area_loss_bool=new_bboxes_area_loss_bool,
                                            model_size=model_size)

adv_img = uap_phantom_sponge_attack.run_attack()