import argparse
import torch
import os
import random
import numpy
from datasets.augmentations1 import train_transform
from datasets.split_data_set_combined import SplitDatasetCombined_BDD
from attack.uap_phantom_sponge import UAPPhantomSponge
import numpy

def parse_args():
    parser = argparse.ArgumentParser(description='Your script description')
    parser.add_argument('--models_vers', nargs='+', type=int, default=[5], help='Model versions')
    parser.add_argument('--epsilon', type=int, default=30, help='Epsilon value')
    parser.add_argument('--lambda_1', type=float, default=0.60, help='Lambda 1 value')
    parser.add_argument('--lambda_2', type=int, default=10, help='Lambda 2 value')
    parser.add_argument('--seed', type=int, default=42, help='Random seed')
    parser.add_argument('--patch_size', nargs=2, type=int, default=[640, 640], help='Patch size')
    parser.add_argument('--img_size', nargs=2, type=int, default=[640, 640], help='Image size')
    parser.add_argument('--batch_size', type=int, default=8, help='Batch size')
    parser.add_argument('--num_workers', type=int, default=4, help='Number of workers')
    parser.add_argument('--max_labels_per_img', type=int, default=65, help='Maximum labels per image')
    parser.add_argument('--new_bboxes_area_loss_bool', action=argparse.BooleanOptionalAction, help='New bboxes area loss flag')
    parser.add_argument('--model_size', type=str, default='l', help='Model size')
    parser.add_argument('--dataset', type=str, default='bdd', help='Dataset')
    return parser.parse_args()

def main():
    args = parse_args()
    
    if args.dataset == 'bdd':
        IMG_DIR = '/scratch/cschoof/datasets/bdd_in_YOLOV5_train_newLabels/images/val'
        LAB_DIR = '/scratch/cschoof/datasets/bdd_in_YOLOV5_train_newLabels/labels/val'
    elif args.dataset == 'mtsd':
        IMG_DIR = '/ceph/csedu-scratch/project/cschoof/MTSD/MTSD/images'
        LAB_DIR = '/ceph/csedu-scratch/project/cschoof/MTSD/MTSD/annotations'
    elif args.dataset == 'pascal-voc':
        IMG_DIR = '/ceph/csedu-scratch/project/cschoof/PASCAL-VOC/images/val'
        LAB_DIR = '/ceph/csedu-scratch/project/cschoof/PASCAL-VOC/labels/val'
    elif args.dataset == 'kitti':
        IMG_DIR = '/ceph/csedu-scratch/project/cschoof/KITTI/valid/images'
        LAB_DIR = '/ceph/csedu-scratch/project/cschoof/KITTI/valid/labels'
    else:
        print("Please provide one of the following three datasets: 'bdd', 'mtsd', 'pascal-voc', or 'kitti'")

    def collate_fn(batch):
        return tuple(zip(*batch))

    def set_random_seed(seed_value, use_cuda=True):
        numpy.random.seed(seed_value)  # cpu vars
        torch.manual_seed(seed_value)  # cpu  vars
        random.seed(seed_value)  # Python
        os.environ['PYTHONHASHSEED'] = str(seed_value)  # Python hash buildin
        if use_cuda:
            torch.cuda.manual_seed(seed_value)
            torch.cuda.manual_seed_all(seed_value)  # gpu vars
            torch.backends.cudnn.deterministic = True  # needed
            torch.backends.cudnn.benchmark = False

    split_dataset = SplitDatasetCombined_BDD(
                img_dir= IMG_DIR,
                lab_dir= LAB_DIR,
                max_lab=args.max_labels_per_img,
                img_size=args.img_size,
                transform=train_transform,
                collate_fn=collate_fn)

    train_loader, val_loader, test_loader = split_dataset(val_split=0.1,
                                                        shuffle_dataset=True,
                                                        random_seed=args.seed,
                                                        batch_size=args.batch_size,
                                                        ordered=False,
                                                        collate_fn=collate_fn)

    torch.cuda.empty_cache()

    patch_name = r"yolov"
    for ver in args.models_vers:
        patch_name += f"{ver}"
    patch_name += f"_eps={args.epsilon}_lmbd1={args.lambda_1}_lmbd2={args.lambda_2}_newLoss={args.new_bboxes_area_loss_bool}_modelSize={args.model_size}_dataset={args.dataset}"

    print("-------------------------------------")
    print("SETUP: ")
    print(f"models_vers = {args.models_vers}")
    print(f"epsilon = {args.epsilon}")
    print(f"lambda_1 = {args.lambda_1}")
    print(f"lambda_2 = {args.lambda_2}")
    print(f"new bboxes_area loss term = {args.new_bboxes_area_loss_bool}")
    print(f"model size used (only yolov5 and up) = {args.model_size}")
    print(f"dataset = {args.dataset}")
    print("-------------------------------------")

    uap_phantom_sponge_attack = UAPPhantomSponge(patch_folder=patch_name, 
                                                train_loader=train_loader, 
                                                val_loader=val_loader, 
                                                epsilon = args.epsilon, 
                                                lambda_1=args.lambda_1, 
                                                lambda_2=args.lambda_2, 
                                                patch_size=args.patch_size, 
                                                models_vers=args.models_vers,
                                                new_bboxes_area_loss_bool=args.new_bboxes_area_loss_bool,
                                                model_size=args.model_size)

    adv_img = uap_phantom_sponge_attack.run_attack()

    # The rest of your script goes here
    # Replace your fixed variable assignments with args values

if __name__ == '__main__':
    main()
