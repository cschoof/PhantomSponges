import torch
from torchvision import transforms
from PIL import Image
import random

for i in range(1,2):
    random_scale_w = random.uniform(0.5, 2)
    random_scale_h = random.uniform(0.5, 2)

    # Load the image using PIL
    image_path = "/ceph/csedu-scratch/project/cschoof/bdd_in_YOLOV5_train_newLabels/images/train/b1ca2e5d-84cf9134.jpg"
    image = Image.open(image_path)
    original_img_size = image.size
    image = image.resize((640, 640))

    # Define transformations to convert the image to a torch tensor
    transform = transforms.Compose([transforms.ToTensor()])

    buses = transform(image)

    # Load the image using PIL
    image_path =  "/home/cschoof/experiments/yolov_5_epsilon=30_lambda1=0.6_lambda2=10/final_results/final_patch.png"
    image = Image.open(image_path)
    perturbation = transform(image)

    #print(perturbation.size())

    result = torch.clamp(buses + perturbation, 0, 1)

    # Assuming you have a torch tensor named torch_image
    # You can resize the tensor to (H, W, C) shape using transpose and multiply by 255 (if it's not already in the correct range)
    #pil_image = transforms.ToPILImage()(result.squeeze().cpu() * 255)
    tensor_min = torch.min(result)
    tensor_max = torch.max(result)
    normalized_tensor = (result - tensor_min) / (tensor_max - tensor_min)

    # Convert the normalized tensor to a PIL image
    pil_image = transforms.ToPILImage()(normalized_tensor.squeeze().cpu())
    #print(original_img_size[0], original_img_size[1])
    width = int(640) #* random_scale_w
    height = int(640) #* random_scale_h
    #print(width, height)
    pil_image = pil_image.resize((width, height))

    # Save the PIL image
    image_path = f"/ceph/csedu-scratch/project/cschoof/PhantomSponges/random_resize/perturbed_image_2_randomResize_#{i}.jpg"
    pil_image.save(image_path)