import torch
import torch.nn.functional as F
from PIL import Image
import numpy as np
from local_yolos.yolov5.models.experimental import attempt_load
from local_yolos.yolov5.utils.general import non_max_suppression
from torchvision import transforms
from local_yolos.yolov5.utils.metrics import box_iou
from local_yolos.yolov5.utils.plots import Annotator
import time
from tqdm import tqdm
import os
import random
from datasets.augmentations1 import train_transform
from datasets.split_data_set_combined import SplitDatasetCombined_BDD

from local_yolos.yolov8.ultralytics.nn.tasks import attempt_load_weights



print("Starting script", flush=True)
models_vers = [5] # for example: models_vers = [5] or models_vers = [3, 4, 5]
epsilon = 30 
lambda_1 = 0.6 #1
lambda_2 = 10 #10
seed = 42
patch_size=(640,640)
img_size=(640,640)
batch_size = 8
num_workers = 4
max_labels_per_img = 65
BDD_IMG_DIR = '/ceph/csedu-scratch/project/cschoof/bdd_in_YOLOV5_val/images/val'
BDD_LAB_DIR = '/ceph/csedu-scratch/project/cschoof/bdd_in_YOLOV5_val/labels/val'

def collate_fn(batch):
    return tuple(zip(*batch))

def set_random_seed(seed_value, use_cuda=True):
    numpy.random.seed(seed_value)  # cpu vars
    torch.manual_seed(seed_value)  # cpu  vars
    random.seed(seed_value)  # Python
    os.environ['PYTHONHASHSEED'] = str(seed_value)  # Python hash buildin
    if use_cuda:
        torch.cuda.manual_seed(seed_value)
        torch.cuda.manual_seed_all(seed_value)  # gpu vars
        torch.backends.cudnn.deterministic = True  # needed
        torch.backends.cudnn.benchmark = False

split_dataset = SplitDatasetCombined_BDD(
            img_dir= BDD_IMG_DIR,
            lab_dir= BDD_LAB_DIR,
            max_lab=max_labels_per_img,
            img_size=img_size,
            transform=train_transform,
            collate_fn=collate_fn)

train_loader, val_loader, test_loader = split_dataset(val_split=0.1,
                                                      shuffle_dataset=True,
                                                      random_seed=seed,
                                                      batch_size=batch_size,
                                                      ordered=False,
                                                      collate_fn=collate_fn)



# Load the YOLOv5 model
##model = attempt_load(weights='/ceph/csedu-scratch/project/cschoof/PhantomSponges/local_yolos/yolov5/weights/yolov5s.pt').eval()
#model = attempt_load_weights('yolov8n.pt', device=torch.device('cuda'), inplace=True, fuse=True).to('cuda')

from attacks_tools.yolov8_wrapper import CustomPredictor
#   ONDERSTAANDE REGEL IS ALLEEN VOOR YOLOV8
model = CustomPredictor(weights='local_yolos/yolov8/weights/yolov8n.pt').to('cpu')




def perturb_img(img_name, change_aspect_ratio = True, output_type = "torch"):
    image_path = "/ceph/csedu-scratch/project/cschoof/bdd_in_YOLOV5_val/images/val/" + img_name
    image = Image.open(image_path)
    image = image.resize((640, 640))
    transform = transforms.Compose([transforms.ToTensor()])
    clean_img = transform(image)

    # Load the image using PIL
    #patch_path =  "/home/cschoof/experiments/yolov_5_epsilon=30_lambda1=0.6_lambda2=10/final_results/final_patch.png"
    patch_path =  "/home/cschoof/experiments/yolov_8_eps=30_lmbd1=0.6_lmbd2=10_newLoss=True_modelSize=n_dataset=bdd/final_results/final_patch.png"
    patch = Image.open(patch_path)
    patch = transform(patch)

    perturbed_img = torch.clamp(clean_img + patch, 0, 1)

    # Assuming you have a torch tensor named torch_image
    # You can resize the tensor to (H, W, C) shape using transpose and multiply by 255 (if it's not already in the correct range)
    #pil_image = transforms.ToPILImage()(result.squeeze().cpu() * 255)
    tensor_min = torch.min(perturbed_img)
    tensor_max = torch.max(perturbed_img)
    normalized_tensor = (perturbed_img - tensor_min) / (tensor_max - tensor_min)

    # Convert the normalized tensor to a PIL image
    pil_image = transforms.ToPILImage()(normalized_tensor.squeeze().cpu())
    if change_aspect_ratio:
        random_scale_w = random.uniform(0.8, 0.95)
        #random_scale_h = random.uniform(1, 2.5)
        width = int(640 * random_scale_w)
        width = (width // 32) * 32 #ensures that the new width is a multiple of 32, otherwise the model does not work
        height = 640 #height = int(640 * random_scale_h)
        height = (height // 32) * 32
        pil_image = pil_image.resize((width, height))

    if output_type == "torch":
        perturbed_image = transform(pil_image)
    elif output_type == "numpy":
        perturbed_image = np.array(pil_image)
    elif output_type == "pil":
        pass
    else:
        print("Unsupported output type provided!")

    return(perturbed_image)


def compute_recall(box_iou_output):
    true_pos = 0
    for clean_detection in box_iou_output:
        if clean_detection[clean_detection > 0.45].nelement() != 0:
            true_pos += 1

    return true_pos / box_iou_output.size()[0]


def add_bbs(image, xyxys):
    numpy_clean_img = image.squeeze().permute(1, 2, 0).cpu().numpy()
    annotator = Annotator(np.ascontiguousarray(numpy_clean_img))
    
    for xyxy in xyxys:
        annotator.box_label(xyxy)
    annotator_results = annotator.result()
    annotator_results = (annotator_results * 255).astype(np.uint8)
    pil_image = Image.fromarray(annotator_results)

    return pil_image


def scale_bbs(orig_dims, target_dims, orig_xyxy):
    # Original image dimensions (640x1200)
    original_height, original_width = orig_dims[2:]

    # Target image dimensions (640x640)
    target_height, target_width = target_dims[2:]

    # Calculate scaling factors for x and y dimensions
    x_scale = target_width / original_width
    y_scale = target_height / original_height

    # Scale the coordinates of the bounding boxes
    scaled_bounding_boxes = orig_xyxy.clone().detach()  # Create a copy to preserve the original data
    scaled_bounding_boxes[:, 0] *= x_scale  # Scale x (left)
    scaled_bounding_boxes[:, 1] *= y_scale  # Scale y (top)
    scaled_bounding_boxes[:, 2] *= x_scale  # Scale x2 (right)
    scaled_bounding_boxes[:, 3] *= y_scale  # Scale y2 (bottom)

    return scaled_bounding_boxes




def evaluate_results(loader = test_loader, Tconf = 0.25, Tiou = 0.45, max_det_pert=300, max_det_clean=300):
    all_total_times = []
    all_nms_times = []
    all_num_bbs_before_nms = []
    all_recalls = []

    for image_np, _, image_name in tqdm(loader):
        perturbed_img = perturb_img(image_name[0],change_aspect_ratio = False).unsqueeze(0) 
        clean_img = image_np[0].unsqueeze(0)

        total_times_single_img = []
        nms_times_single_img = []

        for i in range(30):
            start_time_1 = time.time() * 1000
            output_perturbed = model(perturbed_img)[0]
            start_time_2 = time.time() * 1000
            keep_perturbed = non_max_suppression(output_perturbed, Tconf, Tiou, classes=None,max_det=max_det_pert)
            #print(len(keep_perturbed[0]))
            end_time = time.time() * 1000 
            total_time = end_time - start_time_1
            nms_time = end_time - start_time_2
            total_times_single_img.append(total_time)
            nms_times_single_img.append(nms_time)
        
        total_time = sum(total_times_single_img) / len(total_times_single_img)
        nms_time = sum(nms_times_single_img) / len(nms_times_single_img)

        output_clean = model(clean_img)[0]
        keep_clean = non_max_suppression(output_clean, Tconf, Tiou, classes=None,max_det=max_det_clean)

        num_bbs_before_nms_mask = output_perturbed[..., 4] > Tconf 
        num_bbs_before_nms = num_bbs_before_nms_mask.sum().item()   #F(C')

        clean_xyxy = keep_clean[0][:,0:4]
        #print(clean_xyxy)

        #with_bbs = add_bbs(clean_img, clean_xyxy)
        #display(with_bbs)
        perturbed_xyxy = keep_perturbed[0][:,0:4]

        # with_bbs = add_bbs(perturbed_img, perturbed_xyxy)
        # display(with_bbs)


        #scaled_perturbed_xyxy = scale_coords(perturbed_img.shape[2:], perturbed_xyxy, clean_img.shape[2:]).round()
        scaled_perturbed_xyxy = scale_bbs(orig_dims=perturbed_img.shape, target_dims=clean_img.shape, orig_xyxy = perturbed_xyxy)

        # resized_perturbed_img = F.interpolate(perturbed_img, size=(640,640), mode='bilinear', align_corners=False)
        # with_bbs = add_bbs(resized_perturbed_img, scaled_perturbed_xyxy)
        # display(with_bbs)

        box_iou_output = box_iou(clean_xyxy, scaled_perturbed_xyxy)
        
        #check if its the case that there aren't any clean detections
        #in that case, skip computing the recall
        #"the number of original objects detected in the perturbed image"
        #if there aren't any original objects, no recall can be calculated, thus we skip
        if clean_xyxy.nelement() != 0:
            #print(box_iou_output)
            #break
            recall = compute_recall(box_iou_output)
            #print(recall)
            all_recalls.append(recall)


        all_total_times.append(total_time)
        all_nms_times.append(nms_time)
        all_num_bbs_before_nms.append(num_bbs_before_nms)
        
    avg_total_time = sum(all_total_times) / len(all_total_times)
    avg_nms_time = sum(all_nms_times) / len(all_nms_times)
    avg_num_bbs_before_nms = sum(all_num_bbs_before_nms) / len(all_num_bbs_before_nms)
    avg_recall = sum(all_recalls) / len(all_recalls)

    return round(avg_total_time, 1), round(avg_nms_time, 1), round(avg_num_bbs_before_nms), round(avg_recall, 3)

results = evaluate_results()

print(results)