import torch

# try:
#     total_size = 0
#     total_elements = 0
#     step_size = 50000  # Adjust the step size as needed
#     while True:
#         # Attempt to allocate a tensor of the current size
#         tensor = torch.zeros((step_size, step_size)).cuda()
#         total_size += tensor.element_size() * tensor.nelement() / (1024 ** 2)  # Calculate size in MB
#         total_elements += tensor.nelement()
#         print(f"Allocated {total_size:.2f} MB of GPU memory.", flush=True)
#         print(f"{total_elements}", flush=True)
# except RuntimeError as e:
#     print(f"Error: {e}", flush=True)


try:
    tensor = torch.zeros((100000,100000)).cuda()
    print("it succeeded")
except RuntimeError as e:
    print('UH OH, STINKY!', flush=True)
    tensor = torch.zeros((5000,5000)).cuda()
    print("we can still make other tensors")