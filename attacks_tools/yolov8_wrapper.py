from local_yolos.yolov8.ultralytics.nn.autobackend import AutoBackend
import torch
import torch.nn as nn

# Define a wrapper class for the DetectionModel
class CustomDetectionModelWrapper(nn.Module):
    def __init__(self, detection_model, device = "cuda", output_format = 'yolov8'):
        super(CustomDetectionModelWrapper, self).__init__()
        self.detection_model = detection_model.to(device)
        self.device = device
        self.output_format = output_format

    def forward(self, input):
        # Perform the forward pass using the original model
        output = self.detection_model(input)

        # Add your post-processing logic here
        processed_output = self.custom_post_processing(output, self.device, self.output_format)

        return processed_output

    def custom_post_processing(self, output, device, output_format):
        if output_format == 'yolov5':
            preds = list(output)
            preds[0] = preds[0].permute(0,2,1) #make sure its similar to yolov5's output (torch.Size([8, 25200, 85]))
            new_shape = list(preds[0].shape)
            new_shape[-1] += 1
            new_tensor = torch.zeros(new_shape).to(device)

            new_tensor[:, :, 4] = 1 
            new_tensor[:, :, 0:4], new_tensor[:, :, 5:] = preds[0][:, :, 0:4], preds[0][:, :, 4:] 
            return (new_tensor, )
        elif output_format == 'yolov8':
            return (output[0], )
