import torch 
from local_yolos.yolov6.hubconf import Detector
from local_yolos.yolov6.yolov6.utils.nms import non_max_suppression
from local_yolos.yolov6.yolov6.layers.common import DetectBackend


class CustomDetector(Detector):
    def __init__(self, ckpt_path, class_names, device, img_size=640, conf_thres=0.25, iou_thres=0.45, max_det=1000):
        super().__init__(ckpt_path, class_names, device, img_size, conf_thres, iou_thres, max_det)

    def forward(self, x):
        #src_shape = (x.shape[2], x.shape[3])
        pred_results = super(Detector, self).forward(x)
        # classes = None  # the classes to keep
        # det = non_max_suppression(pred_results, self.conf_thres, self.iou_thres,
        #                           classes, agnostic=False, max_det=self.max_det)[0]
        # det[:, :4] = Inferer.rescale(
        #     x.shape[2:], det[:, :4], src_shape).round()
        # boxes = det[:, :4]
        # scores = det[:, 4]
        # labels = det[:, 5].long()
        # prediction = {'pre_nms': pred_results,
        #             'final': det,
        #             'boxes': boxes, 
        #             'scores': scores,  
        #             'labels': labels}
        return (pred_results, )

class CustomDetectBackend(DetectBackend):
    def __init__(self, weights, device):
        super().__init__(weights, device)

    def forward(self, x):
        #src_shape = (x.shape[2], x.shape[3])
        pred_results = super().forward(x)
        return (pred_results, )